package examenleprii.modelo;

import examenleprii.controlador.ServiciosContacto;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

//Clase de exrtension AbstactTableModel para la tabla
public class ModeloTabla extends AbstractTableModel {
    //Arraylist de tipo Contacto
    ArrayList<Contacto> listaContactos = new ArrayList<>();
    //Vector con los nombres de las columnas de la tabla
    private String[] columna = {"Nombre", "Direccion", "Telefono", "Observacion"};

    public ModeloTabla() {
        //la lista obtiene los valores de la lista de la clase ServiciosContactos
        listaContactos= ServiciosContacto.getLIST_CONTACTOS();
    }

    //Metodosos abtractos
    @Override
    public int getRowCount() {
        return listaContactos.size();
    }

    @Override
    public int getColumnCount() {
        return columna.length;
    }
    //Metodo para agregar los datos de la lista en la tabla
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return listaContactos.get(rowIndex).getNombre();
            case 1:
                return listaContactos.get(rowIndex).getDireccion();
            case 2:
                return listaContactos.get(rowIndex).getTelefono();
            case 3:
                return listaContactos.get(rowIndex).getObservaciones();
            default:
                return null;

        }
    }

    @Override
    public String getColumnName(int column) {
        return columna[column];
    }

}
