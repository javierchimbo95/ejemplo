/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenleprii.vista;

import examenleprii.controlador.ServiciosContacto;
import examenleprii.controlador.DatosException;
import examenleprii.controlador.ValidacionDatos;
import examenleprii.modelo.ModeloTabla;
import javax.swing.JOptionPane;

/**
 *
 * @author DARWIN
 */
public class Ventana extends javax.swing.JFrame {

    int seleccion;//Variable que almacera la posicion de los datos de la tabla

    public Ventana() {

        initComponents();
        this.setTitle("Agenda");//Titulo de JFrame
        this.setLocationRelativeTo(this);//Centra la pantalla
        estadoBotones("nuevo");//inicia solo con el boton nuevo ACTIVADO
        jTableContacto.setModel(new ModeloTabla());//Obtiene el modelo de la clase ModeloTabla
    }

    public Ventana(String titulo) {
        super("Agenda");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jtfNombre = new javax.swing.JTextField();
        jtfObservaciones = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        btnActualizar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableContacto = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jtfTelefono = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jtfDireccion = new javax.swing.JTextField();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenuArchivo = new javax.swing.JMenu();
        jMenuItemSalir = new javax.swing.JMenuItem();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jtfNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jtfObservaciones.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtfObservaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfObservacionesActionPerformed(evt);
            }
        });

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnBorrar.setText("Borrar");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jTableContacto.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTableContacto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jTableContacto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableContactoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableContacto);
        if (jTableContacto.getColumnModel().getColumnCount() > 0) {
            jTableContacto.getColumnModel().getColumn(0).setHeaderValue("Title 1");
            jTableContacto.getColumnModel().getColumn(1).setHeaderValue("Title 2");
            jTableContacto.getColumnModel().getColumn(2).setHeaderValue("Title 3");
            jTableContacto.getColumnModel().getColumn(3).setHeaderValue("Title 4");
        }

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Nombre:");

        jtfTelefono.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtfTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfTelefonoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Dirección:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Teléfono:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Observaciones:");

        jtfDireccion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtfDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jtfDireccionActionPerformed(evt);
            }
        });

        jMenuArchivo.setText("Archivo");

        jMenuItemSalir.setText("Salir");
        jMenuItemSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSalirActionPerformed(evt);
            }
        });
        jMenuArchivo.add(jMenuItemSalir);

        jMenuBar2.add(jMenuArchivo);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 517, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(77, 77, 77)
                                .addComponent(btnNuevo)
                                .addGap(35, 35, 35)
                                .addComponent(btnBorrar)
                                .addGap(31, 31, 31)
                                .addComponent(btnActualizar)
                                .addGap(29, 29, 29)
                                .addComponent(btnGuardar)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jtfNombre)
                            .addComponent(jtfDireccion)
                            .addComponent(jtfTelefono)
                            .addComponent(jtfObservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 369, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(30, 30, 30))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jtfDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtfObservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(36, 36, 36)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevo)
                    .addComponent(btnBorrar)
                    .addComponent(btnActualizar)
                    .addComponent(btnGuardar))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jtfDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfDireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfDireccionActionPerformed

    private void jtfTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfTelefonoActionPerformed

    private void jtfObservacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jtfObservacionesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jtfObservacionesActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        estadoBotones("guardar");//El boton GUARDAR se activa y el resto se desactiva

    }//GEN-LAST:event_btnNuevoActionPerformed

    private void jMenuItemSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSalirActionPerformed
        System.exit(0);//Finaliza el Programa
    }//GEN-LAST:event_jMenuItemSalirActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed

        ValidacionDatos objetoValidar = new ValidacionDatos(); //Crean un nuevo objeto
        String mensaje = ""; //Variable para mostrar los errores producidos
        boolean ban = false; //Variable logica
        
        //Try-Catch que controla la validacion del Nombre
        try {
            objetoValidar.validadarNombre(jtfNombre.getText());
        } catch (DatosException ex) {
            System.out.println(ex.getMessage());//Muestra el error en consola
            mensaje = mensaje + "*" + ex.getMessage() + "\n";//Concatena el error en la variable
        }
        
        //Try-Catch que controla la validacion de la Direccion
        try {
            objetoValidar.validadarDireccion(jtfDireccion.getText());
        } catch (DatosException ex) {
            System.out.println(ex.getMessage());//Muestra el error en consola
            mensaje = mensaje + "*" + ex.getMessage() + "\n";//Concatena el error en la variable
        }

        //Try-Catch que controla la validacion del Telefono
        try {
            objetoValidar.validarTelefono(jtfTelefono.getText());
        } catch (DatosException ex) {
            System.out.println(ex.getMessage());//Muestra el error en consola
            mensaje = mensaje + "*" + ex.getMessage() + "\n";//Concatena el error en la variable
        }
        //*Condicional que nos ayuda a controlar los errores antes de guardar los datos
        //Si el mensaje esta vacio entonces procede al siguiente paso que es la verificacion de datos repetidos
        if (mensaje.equals("")) {
            
            ServiciosContacto objetoServiciosContacto = new ServiciosContacto();//Nuevo objeto de ServiosContacto
            //La bariable logica almacena el valor que retorna de la calse ServiciosContacto
            ban = objetoServiciosContacto.insertar(jtfNombre.getText(), jtfDireccion.getText(), jtfTelefono.getText(), jtfObservaciones.getText());
            //En el caso de que la variable sea True nos muestra un mensaje de dialog indicandos que el nombre ya se repite
            if (ban == true) {
                JOptionPane.showMessageDialog(this, "El nombre ya existe \n \nPor favor ingrese un nuevo Nombre ");//Muestra el mensaje
            } else {//Caso contrario restablece los botones y actualiza la tabla
                jTableContacto.setModel(new ModeloTabla());
                estadoBotones("nuevo");
            }
        /*En el caso de que se haya produciodo errores cuando se llenaron los datos,
          la variable mensaje contendra todos los errores*/
        } else {
            JOptionPane.showMessageDialog(this, mensaje);//Muestra una ventana de dialogo indicandonos los errores
        }

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void jTableContactoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableContactoMouseClicked
        //Variable que nos indica la posicion del mouse al dar clic en la tabla
        seleccion = jTableContacto.rowAtPoint(evt.getPoint());
        //Los JTextFiel toman los valores de acuerdo a la fila y posicion en la que el mouse haya hecho clic
        jtfNombre.setText(String.valueOf(jTableContacto.getValueAt(seleccion, 0)));
        jtfDireccion.setText(String.valueOf(jTableContacto.getValueAt(seleccion, 1)));
        jtfTelefono.setText(String.valueOf(jTableContacto.getValueAt(seleccion, 2)));
        jtfObservaciones.setText(String.valueOf(jTableContacto.getValueAt(seleccion, 3)));
        estadoBotones("modificarBorrar");//Activa los botones ACTUALIZAR y BORRAR
    }//GEN-LAST:event_jTableContactoMouseClicked

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        ServiciosContacto objetoServiciosContacto = new ServiciosContacto();//Nuevo objeto de ServiosContacto
        objetoServiciosContacto.eliminar(seleccion);//Envia la posicion del dato seleccionado a la clase ServiosContacto
        jTableContacto.setModel(new ModeloTabla());//Se actualiza la tabla 
        estadoBotones("nuevo");//Boton nuevo se Activa
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        ServiciosContacto objetoControlador = new ServiciosContacto();//Nuevo objeto de ServiosContacto
        //Envia la posicion y los datos a actualizar a la clase ServiosContacto
        objetoControlador.actualizar(seleccion, jtfNombre.getText(), jtfDireccion.getText(), jtfTelefono.getText(), jtfObservaciones.getText());
        jTableContacto.setModel(new ModeloTabla());//Actualiza la tabla
        estadoBotones("nuevo");//Boton nuevo se Activa

    }//GEN-LAST:event_btnActualizarActionPerformed
    //Metodo para controlar los botones Nuevo, Actulizar, Borrar, Guardar
    public void estadoBotones(String boton) {
        switch (boton) {
            case "nuevo":
                btnNuevo.setEnabled(true);
                btnActualizar.setEnabled(false);
                btnGuardar.setEnabled(false);
                btnBorrar.setEnabled(false);
                jtfNombre.setEnabled(false);
                jtfNombre.setText("");
                jtfDireccion.setEnabled(false);
                jtfDireccion.setText("");
                jtfTelefono.setEnabled(false);
                jtfTelefono.setText("");
                jtfObservaciones.setEnabled(false);
                jtfObservaciones.setText("");
                break;
            case "modificarBorrar":
                btnNuevo.setEnabled(false);
                btnActualizar.setEnabled(true);
                btnGuardar.setEnabled(false);
                btnBorrar.setEnabled(true);
                jtfNombre.setEnabled(true);
                jtfDireccion.setEnabled(true);
                jtfTelefono.setEnabled(true);
                jtfObservaciones.setEnabled(true);
                break;
            case "guardar":
                btnNuevo.setEnabled(false);
                btnActualizar.setEnabled(false);
                btnGuardar.setEnabled(true);
                btnBorrar.setEnabled(false);
                jtfNombre.setEnabled(true);
                jtfDireccion.setEnabled(true);
                jtfTelefono.setEnabled(true);
                jtfObservaciones.setEnabled(true);
                break;
            case "borrar":
                btnNuevo.setEnabled(true);
                btnActualizar.setEnabled(false);
                btnGuardar.setEnabled(false);
                btnBorrar.setEnabled(false);
                break;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventana().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenuArchivo;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItemSalir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTableContacto;
    private javax.swing.JTextField jtfDireccion;
    private javax.swing.JTextField jtfNombre;
    private javax.swing.JTextField jtfObservaciones;
    private javax.swing.JTextField jtfTelefono;
    // End of variables declaration//GEN-END:variables
}
