package examenleprii.controlador;
//Clase que va a contener todos los errores (Validaciones)
public class ValidacionDatos {
    //Constructor
    public ValidacionDatos() {

    }

    //Metodo para validar que el nombre contenga caracteres alfabeticos
    public void validadarNombre(String nombre) throws DatosException {
        int aux = 0;//Variable auxiliar

        /*Bucle que recorre la cadena comparanado cada caracter*/
        for (int i = 0; i < nombre.length(); i++) {
            char c = nombre.charAt(i);
            //Condicional para comparar cada caracter
            if ((c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A')) {

            } else {
                /*En el caso de que se encuentro un caracter que no sea parte del
                alfabeto el auxiliar se incrementa*/
                aux++;
            }
        }
        /*Si el auxiliar no es igual a cero entonces se lanza un ERROR*/
        if (aux != 0) {
            throw new DatosException("El nombre solo puede contener caracteres alfabeticos");
        }
    }

    //Metodo para validar que la direccion contenga caracteres alfanumericos
    public void validadarDireccion(String direccion) throws DatosException {
        int aux = 0;//Variable

        /*Bucle que recorre la cadena comparanado cada caracter*/
        for (int i = 0; i < direccion.length(); i++) {
            char c = direccion.charAt(i);
            //Condicional para comparar cada caracter
            if ((c <= 'z' && c >= 'a') || (c <= 'Z' && c >= 'A') || (c <= '9' && c >= '0')) {

            } else {
                /*En el caso de que se encuentro un caracter que no sea parte del
                alfanumerico el auxiliar se incrementa*/
                aux++;
            }
        }
        /*Si el auxiliar no es igual a cero entonces se lanza un ERROR*/
        if (aux != 0) {
            throw new DatosException("La direccion solo puede contener caracteres alfanumericos");
        }
    }

    //Metodo para validar que el telfono contenga valores numericos
    public void validarTelefono(String telefono) throws DatosException {
        int aux = 0;//Variable

        /*Bucle que recorre la cadena comparanado cada caracter*/
        for (int i = 0; i < telefono.length(); i++) {
            char c = telefono.charAt(i);
            //Condicional para comparar cada caracter
            if (c <= '9' && c >= '0') {

            } else {
                /*En el caso de que se encuentro un caracter que no sea parte del
                alfanumerico el auxiliar se incrementa*/
                aux++;
            }
        }
        /*Si el auxiliar no es igual a cero entonces se lanza un ERROR*/
        if (aux != 0) {
            throw new DatosException("El numero de telefono solo puede contener valores numericos");
        }else{
            //controla que el numero de telefono no exceda los 10 digitos
            if(telefono.length()>10){
                throw  new DatosException("El numero de telefono solo pude contener un maximo de 10 digitos");
            }
        }
    }
}
