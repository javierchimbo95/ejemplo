
package examenleprii.controlador;

//Exception crea se extiende de la clase padre Exception
public class DatosException extends Exception {
    public DatosException(String error){
        super(error);
    }
}
