package examenleprii.controlador;

import examenleprii.modelo.Contacto;
import examenleprii.modelo.ModeloTabla;
import java.util.ArrayList;

//Clase que permite el mantenimiento del programa
public class ServiciosContacto {

    Contacto objetoContacto = new Contacto();//objeto de tipo Contacto
    static ArrayList<Contacto> LIST_CONTACTOS = new ArrayList<>();//Lista estatica que almacena los datos de tipo Contacto
    ModeloTabla objetoTabla = new ModeloTabla();//Objeto de tipo ModeloTabla

    //Constructor
    public ServiciosContacto() {

    }
    
    //Funcion que nos retorna True en el caso de que no se haya insertado el dato
    //O false en el caso de que se haya guardado los datos
    //La funcion obtiene los datos de JFrame
    public boolean insertar(String nombre, String direccion, String telefono, String observacion) {
        objetoContacto = new Contacto();//Inicializacion del Objeto
        //Asignacion de llos datos al objetoContacto
        objetoContacto.setNombre(nombre);
        objetoContacto.setDireccion(direccion);
        objetoContacto.setTelefono(telefono);
        objetoContacto.setObservaciones(observacion);
        
        //Condicional que nos permite controlar si existe el dato o no
        //Para la cual envia el objetoContacto a la funcion validarNombre
        //en el caso de que exista este retornara True hacia el JFrame
        if (validarNombre(objetoContacto)) {
            return true;
            //Caso contrario agrega el objetoContacto a la lista y retorna False hacia el JFrame
            
        } else {
            LIST_CONTACTOS.add(objetoContacto);
            return false;
        }

    }
    
    //Metodo que permite actualizar los doatos.. Obtiene  la posicion y los datos a actualizar
    public void actualizar(int posicion, String nombre, String direccion, String telefono, String observacion) {
        objetoContacto = new Contacto();
        objetoContacto.setNombre(nombre);
        objetoContacto.setDireccion(direccion);
        objetoContacto.setTelefono(telefono);
        objetoContacto.setObservaciones(observacion);
        LIST_CONTACTOS.set(posicion, objetoContacto);
    }
    
    //Metodo para eliminar un objeto de tipo Contactos de la lista
    //Obtiene la posicion del objeto a eliminar
    public void eliminar(int posicion) {
        LIST_CONTACTOS.remove(posicion);
    }

    //Funcion que permite comparar el objeto con los de la lista antes de guardar en la lista
    public boolean validarNombre(Contacto objetoContacto) {
        boolean ban = false;//variable de tipo logico
        for (int i = 0; i < LIST_CONTACTOS.size(); i++) {//El bucle recorre la lista
            if (objetoContacto.equals(LIST_CONTACTOS.get(i))) {//Compara los objetos
                ban = true;//Si el ddato existe la variable ban se le asigna TRUE
                if (ban == true) {
                    break;//Sale del bucle
                }
            }
        }
        return ban;//retorna la varibale ban
    }

    //Metodos getter y setter de la lista de contactos
    public static ArrayList<Contacto> getLIST_CONTACTOS() {
        return LIST_CONTACTOS;
    }

    public static void setLIST_CONTACTOS(ArrayList<Contacto> LIST_CONTACTOS) {
        ServiciosContacto.LIST_CONTACTOS = LIST_CONTACTOS;
    }

}
